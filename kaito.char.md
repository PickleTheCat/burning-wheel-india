# Character Index
---
**Name**  Kintin the sailor	**Stock** Human		**Age** 17 	**Lifepaths** 3
**Alias** Furukawa Kaito	**Homeland** Rising Sun Empire	**Features** tiny oni horns, brown eyes, wild hair
---
## Traits
|---------------|---------------|----------|
| Character     | Die           | Call-on  |
|---------------|---------------|----------|
| Tall          | Weather sense | Sea Legs |
| Tiny Horns    |               | bruiser  |
| Brash         |               |          |
| Drunk         |               |          |
| Forked tongue |               |          |
|---------------|---------------|----------|

## Stats
|------------|----|-------|----|---------|----|
| Will       | B3 | Power | B6 | Agility | B3 |
|------------|----|-------|----|---------|----|
| Perception | B4 | Forte | B5 | Speed   | B4 |
|------------|----|-------|----|---------|----|

## Attributes
|---------|----|------------|----|---------------|-----|
| Health  | B4 | Corruption | B2 | Reflexes      | B3  |
|---------|----|------------|----|---------------|-----|
| Steel   | B5 |            |    | Mortal Wounds | B11 |
|---------|----|------------|----|---------------|-----|
| Circles | B1 | Resources  | B0 | Cash Dice     | 1d  |
|---------|----|------------|----|---------------|-----|

## Skills
|------------|----|----------------|----|
| Seamanship | B2 | Sea-wise       | B2 |
|------------|----|----------------|----|
| Rigging    | B2 | Spirit-binding | B2 |
|------------|----|----------------|----|
| Knots      | B3 | Astrology      | B2 |
|------------|----|----------------|----|
| Brawling   | B4 | Intimidate     | B2 |
|------------|----|----------------|----|
| Firearms   | B3 |
|------------|----|

## Artha & Epiphanies
|------|---|---------|---|-------|--|
| Fate | 2 | Persona | 2 | Deeds |  |
|------|---|---------|---|-------|--|
