# Name: Furukawa Kaito 布留川 海斗
---
## Session 1

### Beliefs
- There must be a reason the spirits brought us together. (f)
- The court wizard has shown me kindness & should be repaid. I shall protect him next chance that I have.
- I am an outsider to this city.I will ~~either fit in or~~ break it open. (f)

- Some things may never be taken back.

### Instincts
- When struck strike back twice
- If the court wizard is in danger, then interpose myself.
---
## Session 2

### Beliefs
- This storm was released due to my arrogance and must be rebound by my hand. I must find a sacrifice worthy of such a binding. (p)
- The honey witch is the only reasonable person in this crew. I will show her the wisdom of the sea by asking the spirit of this sea her question. (f)
- Some things may never be taken back, and attacking civilians is one of them. I will find who gave the order.

### Instincts
- When struck strike back twice
- If the crew is in danger, then interpose myself.
- My gun is always loaded and ready to fire.
---
## Session 3

### Beliefs
- This storm was released due to my arrogance and must be rebound by my hand. I will make my sacrifice and bind the storm. (p)
- The honey witch is the only reasonable person in this crew. I will make sure she survives what is coming.
- Some things may never be taken back, and attacking civilians is one of them. But the invaders have not. I will find out their motives. (p)

### Instincts
- When struck strike back twice
- If the crew is in danger, then interpose myself.
- My gun is always loaded and ready to fire.
---
## Session 4

### Beliefs
- I know what I am willing to fight for. I will find out what my companions think is worth fighting for. (f)
- The honey witch is the only reasonable person in this crew. I will make sure she survives what is coming.
- Some things may never be taken back, and attacking civilians is one of them. I will find who gave the order.

### Instincts
- When struck strike back twice
- My gun is always loaded and ready to fire.
- When crew members are threatened, react in kind.
---
## Session 5

### Beliefs
- I am going to fight & kill for this city. I will win this duel, one way or another. (f)
- The honey witch is the only reasonable person in this crew. I will make sure she survives what is coming.

### Instincts
- When struck strike back twice
- My gun is always loaded and ready to fire.
- When crew members are threatened, react in kind.
---
## Session 6

### Beliefs
- I am going to fight & kill for this city. I will win this duel, one way or another.
- I will gain my mother's approval. She will see how strong I have become.
- Eveything I do I do for others. There is nothing that I wwon't sacrifice to protect others.

### Instincts
- When struck strike back twice
- My gun is always loaded and ready to fire.
- When crew members are threatened, react in kind.
